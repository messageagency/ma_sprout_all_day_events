INTRODUCTION
------------

This module adds support for timeless (all day) event dates. Two new fields are added to the Event content type for the start and end dates to mark them as timeless. If checked, the time portion of the field form widget is hidden and appropriate times are saved to the database.