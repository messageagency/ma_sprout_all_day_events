(function($, Drupal) {

  // Event date fields time optional.
  // Based off js from https://www.drupal.org/project/date_all_day, which we should probably use in the future.
  Drupal.behaviors.afp_custom = {
    attach: function (context, settings) {
      $('#edit-field-event-date-start-timeless-wrapper > .form-item').detach().appendTo('#edit-field-event-date-start-0 > .fieldset-wrapper');
      $('#edit-field-event-date-end-timeless-wrapper > .form-item').detach().appendTo('#edit-field-event-date-end-0 > .fieldset-wrapper');

      var start_time = $('[name$="field_event_date_start[0][value][time]"]');
      var start_checkbox = $('[name$="field_event_date_start_timeless[value]"]');
      var end_time = $('[name$="field_event_date_end[0][value][time]"]');
      var end_checkbox = $('[name$="field_event_date_end_timeless[value]"]');

      start_checkbox.change(function () {
        changeCheckbox(start_time, start_checkbox, 'start')
      }).trigger('change');
      end_checkbox.change(function () {
        changeCheckbox(end_time, end_checkbox, 'end')
      }).trigger('change');

      function changeCheckbox(time, checkbox, type){
        if (type === 'start') {
          var time_value = '00:00:00';
        }
        else if (type === 'end') {
          var time_value = '23:59:59';
        }
        else {
          throw 'type parameter should have "start" or "end" values';
        }

        if ($(checkbox).is(':checked')) {
          $(time).val(time_value);
          $(time).hide();
        }
        else {
          $(time).show();
        }
      }

    }
  };

})(jQuery, Drupal, drupalSettings);